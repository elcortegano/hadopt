#!/bin/sh

#SBATCH -N 1
#SBATCH --qos=shared
#SBATCH -p shared
#SBATCH -t 4-04:00:00
#SBATCH --cpus-per-task=1
#SBATCH --array=1-10

module load gcc/6.4.0

#managament.sh script for the managament by Metapop of the subdivided populations
# Run just one Replicate of the subdivided populations.
# This script is intended to be launched as job array.

# metapop input files are in $CASE/subpops/genotype.bp_XXX.dat.mtp
# metapop config template is in $TEMPLATES/configMTP


#Some variables
CASE=$1
METHOD=$2  #Method as the config_* template to use: OA OA2 OH OH2 RND
POP=$SLURM_ARRAY_TASK_ID  #Number of Base population ($JOB_ARRAY_ID when lauched as job array)
GENS=$3
REPS=$4
TYPE=$5 #Maker type: ms snp hap
FLAG=$6

# el directorio de inicio debe de ser una ruta absoluta, de forma que el script pueda ser lanzado desde cualquier ubicacion
WD=$PWD
SUBPOPS=${WD}/${CASE}/subpops
OUT=${STORE}/${CASE}/${TYPE}
mkdir -p ${OUT}
TEMPLATES=${WD}/templates
SCRATCH=$LUSTRE

#Setting template for number of replicates in the output filenames
# get the number of replicate digits in the input files
DG=$(($(ls $SUBPOPS/genotype.bp_*.${TYPE}.mtp | head -1 | cut -f2 -d"_" | cut -f1 -d"." | wc -m)-1))
INDEX=$(printf "%0${DG}d" $POP)
cd $SCRATCH
mkdir -p ${CASE}/${METHOD}/${TYPE}
cd $CASE/${METHOD}/${TYPE}

sed -e "s/SETreps/${REPS}/" -e "s/SETgens/${GENS}/" -e "s/SETname/${TYPE}_${METHOD}_${INDEX}_${FLAG}/" $TEMPLATES/config_${METHOD} > ${SCRATCH}/${CASE}/${METHOD}/${TYPE}/config_${TYPE}_${METHOD}_${INDEX}_${FLAG}

#Using metapop directly from the working dir
${WD}/metapop --config=config_${TYPE}_${METHOD}_${INDEX}_${FLAG} --mtp $SUBPOPS/genotype.bp_${INDEX}.${TYPE}.mtp 

mkdir -p $OUT
mv ${SCRATCH}/${CASE}/${METHOD}/${TYPE}/res_${TYPE}_${METHOD}_${INDEX}_${FLAG}_* $OUT/
rm config_${TYPE}_${METHOD}_${INDEX}_${FLAG}
