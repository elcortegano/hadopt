#!/bin/sh

#SBATCH --qos=shared
#SBATCH -p shared
#SBATCH -t 4-00:00:00
#SBATCH --array=1-10
#SBATCH --mail-user e.lopez@uvigo.es
#SBATCH --mail-type ALL

module load gcc/6.4.0

# slim.sh
# Script to generate the base populations for the SNPs and HAplotype markers using slim
# Different scenarios are called using different SLiM's config files in TEMPLATES
# This script is intended to be launched as job array.

if [ "$#" -ne 1 ]; then
	echo "Ilegal number of parameters."
	exit
fi

#Some variables
CASE=$1 #Template of slim configuration to use (including extension)
POP=$SLURM_ARRAY_TASK_ID #Number of Replicate ($JOB_ARRAY_ID when lauched as job array)
WD=$PWD
SCRATCH=$LUSTRE
OUT=${WD}/${CASE}/subpops
TEMPLATES=${WD}/templates
SLIM_CONFIG="${TEMPLATES}/${CASE}.slim"

if [ ! -f $SLIM_CONFIG ]; then
	echo "SLiM configuration file named $SLIM_CONFIG does not exist"
	exit
fi

#Setting template for number of replicates in the output filenames
# get the number of replicate digits in the input files
INDEX=$(printf "%02d" $POP)

cd $SCRATCH
mkdir -p $CASE
cd $CASE
sed -e "s/SETname/genotype.bp_${INDEX}/" $SLIM_CONFIG > ${SCRATCH}/config_${INDEX}
#Using slim directly from the working dir
${WD}/slim ${SCRATCH}/config_${INDEX}

mkdir -p $OUT
mv ${SCRATCH}/${CASE}/genotype.bp_${INDEX}*.mtp $OUT/
