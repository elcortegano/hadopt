#!/bin/sh

SCENARIO=$1
MANAGE=$2
MARKER=$3
GENERATIONS=25
MTP_REPLICATES=1 # metapop replicates
NRUNS=10 # number of independent runs (replicates)

if [ "$#" -ne 3 ]; then
	echo "Illegal number of parameters"
	exit
fi

if [ "$MANAGE" != "RND" ] && [ "$MANAGE" != "GD" ] && [ "$MANAGE" != "GD2" ] && [ "$MANAGE" != "OA" ] && [ "$MANAGE" != "OA2" ] && [ "$MANAGE" != "AD" ] && [ "$MANAGE" != "AD2" ]; then
	echo "Invalid management method"
	exit
fi

if [ "$MARKER" != "ms" ] && [ "$MARKER" != "hap" ] && [ "$MARKER" != "hap100" ] && [ "$MARKER" != "snp" ]; then
	echo "Invalid marker"
	exit
fi

for i in `seq 1 "$NRUNS"`; do

	sbatch scripts/management2.sh $SCENARIO $MANAGE $GENERATIONS $MTP_REPLICATES $MARKER $i;
done;
