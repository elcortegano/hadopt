#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <regex>
#include <string>

std::vector<std::string> split_string (std::string string_, char character) {

	std::vector<std::string> result;
	std::istringstream iss (string_);
	std::string piece;

	while (getline(iss, piece, character)) result.push_back(piece.c_str());

	return result;
}

template<typename T>
bool is_in (T i, const std::vector<T>& v) {
	return (std::find(v.begin(), v.end(), i) != v.end());
}

int main () {

	int ploidy (2);
	int each (100);
	std::string outfilename ("genotype.bp_10.snp.tmp");

	// Check format and read input parameters
	std::string filename ("genotype.bp_10.snp.ped");
	std::ifstream fr(filename);
	if (!fr.is_open()) { exit(-1); }

	// Read individuals
	std::vector<std::vector<std::string>> v_Inds;
	std::string ind_string;

	while (getline (fr,ind_string)) {
		ind_string = std::regex_replace(ind_string, std::regex("^ +| +$|( ) +"), "$1");
		std::vector<std::string> split_ind (split_string(ind_string, ' '));
		v_Inds.push_back(split_ind);
	}

	int N (v_Inds.size());
	std::vector<std::vector<std::string>> haps;
	for (int i(0); i<N; ++i) {
		std::vector<std::string> inds;
		for (std::size_t j(0); j<v_Inds[i].size(); ++j) {
			inds.push_back(v_Inds[i][j]);
		}
		haps.push_back(inds);
	}

	std::vector<std::vector<std::string>> refined_hap;
	for (std::size_t i(0); i<haps.size(); ++i) {
		std::vector<std::string> refined_ind;
		for (std::size_t j(0); j<6; ++j) {
			refined_ind.push_back(haps[i][j]);
		}
		for (std::size_t j(6); j<haps[i].size(); j += (ploidy *each)) {
			refined_ind.push_back(haps[i][j]);
			refined_ind.push_back(haps[i][j+1]);
		}
		refined_hap.push_back(refined_ind);
	}

	std::ofstream out (outfilename);
	for (std::size_t i(0); i<refined_hap.size(); ++i) {
		out << refined_hap[i][0];
		for (std::size_t j(1); j<refined_hap[i].size(); ++j) {
			out << " " << refined_hap[i][j];
		}
		out << std::endl;
	}

	return 0;
}
