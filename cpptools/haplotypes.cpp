#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <regex>
#include <string>

std::vector<std::string> split_string (std::string string_, char character) {

	std::vector<std::string> result;
	std::istringstream iss (string_);
	std::string piece;

	while (getline(iss, piece, character)) result.push_back(piece.c_str());

	return result;
}

template<typename T>
bool is_in (T i, const std::vector<T>& v) {
	return (std::find(v.begin(), v.end(), i) != v.end());
}

int main () {

	int ploidy(2);
	int group (7);
	std::string outfilename ("cows_haps_7.ped");

	// Check format and read input parameters
	std::string filename ("file.ped");
	std::ifstream fr(filename);
	if (!fr.is_open()) {
		exit(-1);
	}

	// Read individuals
	std::vector<std::vector<std::string>> v_Inds;
	std::string ind_string;

	while (getline (fr,ind_string)) {
		ind_string = std::regex_replace(ind_string, std::regex("^ +| +$|( ) +"), "$1");
		std::vector<std::string> split_ind (split_string(ind_string, ' '));
		v_Inds.push_back(split_ind);
	}

	int N (v_Inds.size());
	std::vector<std::vector<std::string>> haps;
	for (int i(0); i<N; ++i) {
		std::vector<std::string> inds;
		inds.push_back(v_Inds[i][0]);
		inds.push_back(v_Inds[i][1]);
		inds.push_back(v_Inds[i][2]);
		inds.push_back(v_Inds[i][3]);
		inds.push_back(v_Inds[i][4]);
		inds.push_back(v_Inds[i][5]);
		int nalleles ((v_Inds[i].size()-6));
		for (int j(6); j<nalleles; j+=(ploidy*group)) {
			std::string loc1;
			for (int k(0); k<group; ++k) loc1 += v_Inds[i][j+ploidy*k]; 
			std::string loc2;
			for (int k(0); k<group; ++k) loc2 += v_Inds[i][j+ploidy*k+1];
			inds.push_back(loc1);
			inds.push_back(loc2);
		}
		haps.push_back(inds);
	}

	int nloci (haps[0].size()-6);
	
	std::vector<std::vector<std::string>> refined_hap;
	for (int i(0); i<nloci; i+=ploidy) {
		std::vector<std::string> unique_alleles;
		std::vector<int> code;
		int count (1);
		for (std::size_t j(0); j<haps.size(); ++j) {
			std::string a1 (haps[j][6+i]);
			if (!is_in(a1,unique_alleles)) {
				unique_alleles.push_back(a1);
				code.push_back(count);
				haps[j][6+i] = std::to_string(count);
				++count;
			} else {
				for (std::size_t k(0); k<unique_alleles.size(); ++k) {
					if (unique_alleles[k] == a1) {
						haps[j][6+i] = std::to_string(code[k]);
						break;
					}
				}
			}
			std::string a2 (haps[j][6+i+1]);
			if (!is_in(a2,unique_alleles)) {
				unique_alleles.push_back(a2);
				code.push_back(count);
				haps[j][6+i+1] = std::to_string(count);
				++count;
			} else {
					for (std::size_t k(0); k<unique_alleles.size(); ++k) {
					if (unique_alleles[k] == a2) {
						haps[j][6+i+1] = std::to_string(code[k]);
						break;
					}
				}
			}
		}
	}

	std::ofstream out (outfilename);
	for (std::size_t i(0); i<haps.size(); ++i) {
		out << haps[i][0];
		for (std::size_t j(1); j<haps[i].size(); ++j) {
			out << " " << haps[i][j];
		}
		out << std::endl;
	}

	return 0;
}
